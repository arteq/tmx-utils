<?php
/**
 * TMX read and write tests
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\Tmx\Writer;
use ArteQ\Tmx\Reader;

class ReadWriteTest extends TestCase
{
	const READ_FILE = __DIR__.'/read.tmx';
	const WRITE_FILE = __DIR__.'/write.tmx';

	public function setUp()
	{
		@unlink(self::WRITE_FILE);
	}

	public function tearDown()
	{
		@unlink(self::WRITE_FILE);
	}

	public function testCanReadAndWrite()
	{
		// read TMX file
		$reader = new Reader(self::READ_FILE);
		$header = $reader->getHeader();
		$transUnits = $reader->getTransUnits();

		// write same TMX file
		$writer = new Writer(self::WRITE_FILE);
		$writer->setHeader($header);
		$writer->writeStart();
		foreach ($transUnits as $transUnit)
		{
			$tu = $transUnit->getTransUnitElement();
			$writer->writeElement($tu);
		}
		$writer->writeEnd();

		// check written file exists
		$this->assertFileExists(self::WRITE_FILE);

		// check written file is same as read one
		$contentRead = file_get_contents(self::READ_FILE);
		$contentWrite = file_get_contents(self::WRITE_FILE);
		$this->assertEquals($contentRead, $contentWrite);
	}
}