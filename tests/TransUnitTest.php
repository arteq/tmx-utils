<?php
/**
 * TMX trans unit tests
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\Tmx\Reader;

class TransUnitTest extends TestCase
{
	private $tmx;
	private $transUnits;

	public function setUp()
	{
		$tmx = new Reader(__DIR__.'/read.tmx');
		$this->tmx = $tmx;
		$this->transUnits = $tmx->getTransUnits();
	}

	public function testCanGetAllSegments()
	{
		$this->assertCount(3, $this->transUnits);
	}

	public function testCanGetFirstTransUnit()
	{
		$first = $this->transUnits[0]->getTransUnit();

		// check tu attributes
		$this->assertEquals('20200703T155739Z', $first['attributes']['changedate']);
		$this->assertEquals('20200703T155732Z', $first['attributes']['creationdate']);
		$this->assertEquals('budek', $first['attributes']['creationid']);
		$this->assertEquals('budek', $first['attributes']['changeid']);
	}

	public function testCanGetTags()
	{
		$first = $this->transUnits[0]->getTransUnit();
		$tags = $first['tags'];

		$this->assertCount(2, $tags);
		$this->assertArrayHasKey('bpt_1', $tags);
		$this->assertArrayHasKey('ept_1', $tags);

		$this->assertEquals('bpt', $tags['bpt_1']->getName());
		$this->assertEquals('{}', $tags['bpt_1']->getValue());
		$this->assertEquals('❶', $tags['bpt_1']->getPlaceholder());
		$this->assertCount(2, $tags['bpt_1']->getAttributes());
		$this->assertArrayHasKey('i', $tags['bpt_1']->getAttributes());
		$this->assertArrayHasKey('type', $tags['bpt_1']->getAttributes());
	}

	public function testCanGetSegmentsFromTransUnit()
	{
		$first = $this->transUnits[0]->getTransUnit();

		$this->assertEquals('pl', $first['sourceLang']);
		$this->assertEquals('en', $first['targetLang']);
		
		$this->assertEquals('❶1.❷', $first['source']);
		$this->assertEquals('❶1.❷', $first['target']);
	}

	public function testCanGetTransUnitProperties()
	{
		$second = $this->transUnits[1]->getTransUnit();
		$props = $second['children'];

		$this->assertCount(8, $props);
		$this->assertEquals('prop', $props[0]['name']);
		$this->assertEquals('PKP Energetyka', $props[0]['value']);
		$this->assertCount(1, $props[0]['attributes']);
		$this->assertArrayHasKey('type', $props[0]['attributes']);
		$this->assertEquals('client', $props[0]['attributes']['type']);

	}

	public function testCanGetSegmentSource()
	{
		$second = $this->transUnits[1]->getTransUnit();

		$this->assertCount(0, $second['sourceAttributes']);
		$this->assertCount(2, $second['sourceChildren']);
		$this->assertEquals('prop', $second['sourceChildren'][1]['name']);
		$this->assertEquals('<seg>1471.4050.188326.2020</seg>', $second['sourceChildren'][1]['value']);
		$this->assertArrayHasKey('type', $second['sourceChildren'][1]['attributes']);
		$this->assertEquals('x-context-post', $second['sourceChildren'][1]['attributes']['type']);
	}

	public function testCanGetSegmentTarget()
	{
		$second = $this->transUnits[1]->getTransUnit();

		$this->assertCount(0, $second['targetAttributes']);
		$this->assertCount(0, $second['targetChildren']);
	}
}