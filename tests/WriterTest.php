<?php
/**
 * TMX writer tests
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\Tmx\Writer;

class WriterTest extends TestCase
{
	const FILE = __DIR__.'/out.tmx';

	public function setUp()
	{
		@unlink(self::FILE);
	}

	public function tearDown()
	{
		@unlink(self::FILE);
	}

	public function testCanCreate()
	{
		$tmx = new Writer(self::FILE);

		$tmx->set('id-123', 'pl_PL', 'tekst po polsku');
		$tmx->set('id-123', 'en_EN', 'english text');
		$tmx->setAttribute('id-123', 'changedate', '20200102T010203Z');
		$tmx->setAttribute('id-123', 'creationdate', '20200202T010101Z');
		$tmx->setAttribute('id-123', 'creationid', 'user-123');

		$tmx->setProperty('id-123', 'client', 'ACME Ltd.');
		$tmx->write();

		$this->assertFileExists(self::FILE);
		
		$content = file_get_contents(self::FILE);
		$this->assertContains('<tmx version="1.4">', $content);
		
		$this->assertContains('tuid="id-123"', $content);
		$this->assertContains('changedate="20200102T010203Z"', $content);
		$this->assertContains('creationdate="20200202T010101Z"', $content);
		$this->assertContains('creationid="user-123"', $content);
	}

	public function testCanAddSingleSegment()
	{
		$tmx = new Writer(self::FILE);
		$tmx->set('tuid-123', 'pl_PL', 'Tekst polski');
		$tmx->set('tuid-123', 'en_EN', 'English text');

		$data = $tmx->get('tuid-123');

		$this->assertInternalType('array', $data);
		$this->assertCount(2, $data);

		$this->assertArrayHasKey('en_EN', $data);
		$this->assertArrayHasKey('pl_PL', $data);
		$this->assertEquals('English text', $data['en_EN']);
		$this->assertEquals('Tekst polski', $data['pl_PL']);
	}

	public function testCanAddSegmentsArray()
	{
		$rows = [
			['tuid-1', 'pl', 'tekst polski'],
			['tuid-1', 'en', 'english text'],
			['tuid-2', 'pl', 'inny tekst'],
			['tuid-3', 'pl', 'kolejny tekst'],
			['tuid-3', 'en', 'another text'],
		];

		$tmx = new Writer(self::FILE);
		$tmx->setArray($rows);

		$data = $tmx->get();
		$this->assertInternalType('array', $data);
		$this->assertCount(3, $data);

		$row = $tmx->get('tuid-1');
		$this->assertArrayHasKey('en', $row);
		$this->assertArrayHasKey('pl', $row);
		$this->assertEquals('tekst polski', $row['pl']);
	}

	public function testCanCreateStreamed()
	{
		$rows = [
			'tuid-1' => [
				'pl' => 'tekst polski',
				'en' => 'english text',
				'_attributes' => [
					'attr1' => 'value1',
					'attr2' => 'value2',
				],
			],
			'tuid-2' => [
				'pl' => 'inny tekst',
				'_properties' => [
					'type1' => 'value1',
					'type2' => 'value2',
				],
			],
			'tuid-3' => [
				'pl' => 'kolejny tekst',
				'en' => 'another text',
			]
		];

		$tmx = new Writer(self::FILE);

		$tmx->writeStart();
		for ($iter = 1; $iter<10000; $iter++)
		{
			foreach ($rows as $tuid => $tuvs)
			{
				$tmx->writeTu($iter.'-'.$tuid, $tuvs);
			}
		}
		$tmx->writeEnd();

		$this->assertFileExists(self::FILE);
		
		$content = file_get_contents(self::FILE);
		$this->assertContains('<tmx version="1.4">', $content);
		$this->assertContains('tuid="1-tuid-1"', $content);
		$this->assertContains('tuid="100-tuid-2"', $content);
		$this->assertContains('tuid="1000-tuid-3"', $content);
		$this->assertContains('tuid="9999-tuid-3"', $content);
	}

	public function testCanSetHeader()
	{
		$header = [
			'name' => 'header',
			'attributes' => [
				'foo' => 'bar',
				'xxx' => 'yyy'
			],
			'children' => [
				[
					'name' => 'prop', 
					'attributes' => ['attr1' => 'val1', 'attr2' => 'val2'],
					'value' => 'value',
				],
				[
					'name' => 'prop', 
					'attributes' => ['attr11' => 'val11'],
					'value' => 'VaLuE',
				]
			]
		];

		$tmx = new Writer(self::FILE);
		$tmx->setHeader($header);
		$tmx->write();

		$this->assertFileExists(self::FILE);
		
		$content = file_get_contents(self::FILE);
		$this->assertContains('<prop attr1="val1" attr2="val2">value</prop>', $content);
		$this->assertContains('<prop attr11="val11">VaLuE</prop>', $content);
		$this->assertContains('<header foo="bar" xxx="yyy">', $content);
	}
}
