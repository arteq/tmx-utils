<?php
/**
 * TMX reader tests
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\Tmx\Reader;

class ReaderTest extends TestCase
{
	private $tmx;

	public function setUp()
	{
		$tmx = new Reader(__DIR__.'/test.tmx');
		$this->tmx = $tmx;
	}

	public function testCanReadExisting()
	{
		$data = $this->tmx->get();

		$this->assertInternalType('array', $data);
		$this->assertCount(3, $data);

		$this->assertArrayHasKey('test1', $data);
		$this->assertArrayHasKey('test2', $data);
		$this->assertArrayHasKey('test3', $data);
		
		$this->assertArrayHasKey('en_UK', $data['test1']);
		$this->assertEquals('My text', $data['test1']['en_UK']);
	}

	public function testCantReadNonExisting()
	{
		$this->expectException(\Exception::class);
		$tmx = new Reader(__DIR__.'/missing.tmx');
	}

	public function testCanGetByLang()
	{
		$data = $this->tmx->getLang('en_UK');

		$this->assertInternalType('array', $data);
		$this->assertCount(2, $data);

		$this->assertArrayHasKey('test1', $data);
		$this->assertArrayHasKey('test2', $data);

		$this->assertArrayHasKey('en_UK', $data['test1']);
		$this->assertEquals('My text', $data['test1']['en_UK']);
	}

	public function testCanGetByTuid()
	{
		$data = $this->tmx->get('test1');

		$this->assertInternalType('array', $data);
		$this->assertCount(1, $data);

		$this->assertArrayHasKey('en_UK', $data);
		$this->assertEquals('My text', $data['en_UK']);
	}

	public function testCanGetByTuidAndLang()
	{
		$data = $this->tmx->get('test2', 'en_UK');
		$this->assertInternalType('string', $data);
		$this->assertEquals('My second text', $data);

		$data = $this->tmx->get('test3', 'de');
		$this->assertInternalType('string', $data);
		$this->assertEquals('My third text', $data);
	}

	public function testCanRemoveWholeSegment()
	{
		$this->tmx->delete('test2');

		$data = $this->tmx->get();

		$this->assertInternalType('array', $data);
		$this->assertCount(2, $data);

		$this->assertFalse(isset($data['test2']));
	}

	public function testCanRemoveSegmentPart()
	{
		$this->tmx->delete('test2', 'fr_FR');

		$data = $this->tmx->get();

		$this->assertInternalType('array', $data);
		$this->assertCount(3, $data);

		$this->assertTrue(isset($data['test2']['en_UK']));
		$this->assertFalse(isset($data['test2']['fr_FR']));
	}

	public function testCanReadHeader()
	{
		$header = $this->tmx->getHeader();

		$this->assertTrue($header['name'] === 'header');
		$this->assertInternalType('array', $header['attributes']);
		$this->assertInternalType('array', $header['children']);
		$this->assertCount(4, $header['attributes']);
		$this->assertCount(6, $header['children']);

		$this->assertEquals('prop', $header['children'][1]['name']);
		$this->assertEquals('Road Runner', $header['children'][1]['value']);
		$this->assertEquals('defproject', $header['children'][1]['attributes']['type']);
	}

	public function testCanGetLangCodes()
	{
		$tmx = new Reader(__DIR__.'/read.tmx');

		$this->assertEquals('pl', $tmx->getSourceLang());
		$this->assertEquals('en', $tmx->getTargetLang());
	}
}