<?php
/**
 * Splitter tests
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\Tmx\Splitter;

class SplitterTest extends TestCase
{
	private $splitter;

	public function setUp()
	{
		@unlink(__DIR__.'/test_000.tmx');
		@unlink(__DIR__.'/test_001.tmx');
		$this->splitter = new Splitter(__DIR__.'/test.tmx');		
	}

	public function tearDown()
	{
		@unlink(__DIR__.'/test_000.tmx');
		@unlink(__DIR__.'/test_001.tmx');
	}

	public function testCanReadAndSplitTmx()
	{
		$this->splitter->setLimit(2);
		$this->splitter->split();
		$stats = $this->splitter->getStats();

		$this->assertFileExists(__DIR__.'/test_000.tmx');
		$this->assertFileExists(__DIR__.'/test_001.tmx');
		$totalSize = filesize(__DIR__.'/test_000.tmx') + filesize(__DIR__.'/test_001.tmx');

		$this->assertEquals(3, $stats['segmentsWritten']);
		$this->assertEquals(2, $stats['filesWritten']);
		$this->assertEquals($totalSize, $stats['bytesWritten']);
	}
}