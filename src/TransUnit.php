<?php
/**
 * Single TUV translation unit from TMX file
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */
namespace ArteQ\Tmx;

class TransUnit
{
	/**
	 * XML file encoding
	 * @var string
	 */ 
	const ENCODING = 'UTF-8';

	/**
	 * XML document version
	 * @var string
	 */  
	const DOCUMENT = '1.0';

	/**
	 * TMX file version
	 * @var string
	 */ 
	const VERSION = '1.4';

	/**
	 * List of possible inline tags for collapse/expand
	 * @var array
	 */ 
	const TAGS = ['bpt', 'ept', 'sub', 'it', 'ph', 'ut', 'hi', 'mrk', 'x'];

	/**
	 * List of top level <transUnit> attributes
	 * @var array
	 */ 
	protected $attributes = [];

	/**
	 * List of child nodes / parameters
	 * @var array
	 */ 
	protected $children = [];

	/**
	 * List of combined Tag for both source & target elements
	 * @var array
	 */ 
	protected $tags = [];

	/**
	 * Plain text source element representation
	 * @var string
	 */ 
	protected $source = '';
	
	/**
	 * Source language code
	 * @var string
	 */
	protected $sourceLang = '';

	/**
	 * Source element attributes
	 * @var array
	 */  
	protected $sourceAttributes = [];

	/**
	 * Source element child nodes / parameters
	 * @var array
	 */ 
	protected $sourceChildren = [];

	/**
	 * Plain text target element representation
	 * @var string
	 */ 
	protected $target = '';

	/**
	 * Target language code
	 * @var string
	 */ 
	protected $targetLang = '';

	/**
	 * Target element attributes
	 * @var array
	 */ 
	protected $targetAttributes = [];

	/**
	 * Target element child nodes / parameters
	 * @var array
	 */ 
	protected $targetChildren = [];

	/**
	 * Segment marks (ex comments)
	 * @var array
	 */ 
	protected $marks = [];

	/**
	 * Set source & target languages
	 * 
	 * @param string $sourceLang
	 * @param string $targetLang
	 */ 
	public function __construct($sourceLang, $targetLang)
	{
		$this->sourceLang = $sourceLang;
		$this->targetLang = $targetLang;
	}

	/**
	 * Parse translation unit: save attributes, collapse edit marks and memoq internal tags
	 * 
	 * @param \DOMNode $transUnit
	 * @return bool false if could not read tra
	 */ 
	public function read(\DOMNode $transUnit)
	{
		$document = new \DOMDocument(self::VERSION, self::ENCODING);
		$node = $document->importNode($transUnit, true);
		$document->appendChild($node);

		$xpath = new \DOMXPath($document);
		$xpath->registerNamespace('xml', 'XML');

		// read trans-unit attributes
		if ($transUnit->hasAttributes())
		{
			foreach ($transUnit->attributes as $attr)
			{
				$this->attributes[$attr->nodeName] = $attr->nodeValue;
			}
		}

		// read trans-unit properties, except for tuv's
		$childNodes = $xpath->evaluate('*[not(self::tuv)]');
		$this->children = $this->expandChildNodes($childNodes);

		// read source
		$sourceElement = $xpath->evaluate('tuv[@xml:lang="' . $this->sourceLang . '"]/seg')->item(0);
		if (empty($sourceElement))
			return false;

		// read source attributes
		if ($sourceElement->hasAttributes())
		{
			foreach ($sourceElement->attributes as $attr)
			{
				$this->sourceAttributes[$attr->nodeName] = $attr->nodeValue;
			}
		}

		// read source properties, except for seg
		$sourceChildNodes = $xpath->evaluate('tuv[@xml:lang="' . $this->sourceLang . '"]/*[not(self::seg)]');
		$this->sourceChildren = $this->expandChildNodes($sourceChildNodes);

		// $this->collapseEditMarks($this->sourceElement);
		$this->source = $this->collapseTags($sourceElement);

		// read target
		$targetElement = $xpath->evaluate('tuv[@xml:lang="' . $this->targetLang . '"]/seg')->item(0);
		if (empty($targetElement))
			return false;

		if ($targetElement->hasAttributes())
		{
			foreach ($targetElement->attributes as $attr)
			{
				$this->targetAttributes[$attr->nodeName] = $attr->nodeValue;
			}
		}

		// read target properties, except for seg
		$targetChildNodes = $xpath->evaluate('tuv[@xml:lang="' . $this->targetLang . '"]/*[not(self::seg)]');
		$this->targetChildren = $this->expandChildNodes($targetChildNodes);

		// $this->collapseEditMarks($this->targetElement);
		$this->target = $this->collapseTags($targetElement);

		return true;
	}

	/**
	 * Replace memoq inline tags in element with tags placeholders,
	 * store tags in $tags array, store plain text segment in $txt.
	 * 
	 * @param \DOMElement $element
	 * @return string
	 */ 
	public function collapseTags(\DOMElement $element)
	{
		$txt = '';
			
		foreach ($element->childNodes as $node)
		{
			if (in_array($node->nodeName, self::TAGS))
			{
				$tagAttributes = [];
				foreach ($node->attributes as $nodeAttr)
				{
					$tagAttributes[$nodeAttr->nodeName] = $nodeAttr->nodeValue;
				}
				
				$tag = new Tag($node->nodeName, $node->nodeValue, $tagAttributes);
				if (!isset($this->tags[$tag->getHash()]))
				{
					$tag->createPlaceholder(count($this->tags));
					$this->tags[$tag->getHash()] = $tag;
				}
				$txt .= $this->tags[$tag->getHash()]->getPlaceholder();
			}
			elseif ($node->nodeName === '#text')
			{
				$txt .= $node->nodeValue;
			}
		}

		return $txt;
	}

	/**
	 * Expand tags placeholders to DOMElements, prepare final sourceElement/targetElement
	 * 
	 * @param string $field
	 * @return array
	 */ 
	public function expandTags($field)
	{
		if ($field !== 'source' && $field !== 'target')
			throw new \Exception("Tags can be expanded only in 'source' or 'target' elements");

		$children = [];

		// fetch available placeholders
		$placeholders = [];
		foreach ($this->tags as $tag)
		{
			$placeholders[$tag->getPlaceholder()] = $tag;
		}

		$input = $this->{$field};
		$subStr = '';
		for ($poz = 0; $poz < mb_strlen($input); $poz++)
		{
			$char = mb_substr($input, $poz, 1);
			if (isset($placeholders[$char]))
			{
				// add plain text before tag
				if (mb_strlen($subStr) > 0)
					$children[] = ['value' => $subStr];

				// add tag
				$tag = $placeholders[$char];
				$children[] =  ['name' => $tag->getName(), 'value' => $tag->getValue(), 'attributes' => $tag->getAttributes()];

				// clear next plain text substring
				$subStr = '';
			}
			else
			{
				// add char to new plain text substring
				$subStr .= $char;
			}
		}

		// append what's left as plain text 
		if (mb_strlen($subStr) > 0)
			$children[] = ['value' => $subStr];

		return $children;
	}

	/**
	 * Return translation unit as plain array
	 * 
	 * @return array
	 */ 
	public function getTransUnit()
	{
		return [
			'attributes' => $this->attributes,
			'children' => $this->children,
			'tags' => $this->tags,
			'source' => $this->source,
			'sourceLang' => $this->sourceLang,
			'sourceAttributes' => $this->sourceAttributes,
			'sourceChildren' => $this->sourceChildren,
			'target' => $this->target,
			'targetLang' => $this->targetLang,
			'targetAttributes' => $this->targetAttributes,
			'targetChildren' => $this->targetChildren,
		];
	}

	/**
	 * Return translation unit as array with source/target segments injected 
	 * as child nodes, ready to be written by TMX\Writter
	 * 
	 * @return array
	 */ 
	public function getTransUnitElement()
	{
		$tu = $this->getTransUnit();
		$tu['name'] = 'tu';

		// move source to children with attr & props
		$tuv_src = [
			'name' => 'tuv',
			'attributes' => $tu['sourceAttributes'],
			'children' => $tu['sourceChildren'],
		];
		$tuv_src['attributes']['xml:lang'] = $tu['sourceLang'];
		$tuv_src['children'][] = ['name' => 'seg', 'children' => $this->expandTags('source')];

		// move target to children with attr & props
		$tuv_trg = [
			'name' => 'tuv',
			'attributes' => $tu['targetAttributes'],
			'children' => $tu['targetChildren'],
		];
		$tuv_trg['attributes']['xml:lang'] = $tu['targetLang'];
		$tuv_trg['children'][] = ['name' => 'seg', 'children' => $this->expandTags('target')];

		$tu['children'][] = $tuv_src;
		$tu['children'][] = $tuv_trg;

		// unset unneeded keys 
		$keysToUnset = [
			'source', 'sourceLang', 'sourceAttributes', 'sourceChildren',
			'target', 'targetLang', 'targetAttributes', 'targetChildren',
		];
		foreach ($keysToUnset as $key)
		{
			unset($tu[$key]);
		}

		return $tu;
	}

	/**
	 * Set translation unit data from provided array
	 * 
	 * @param array $transUnit
	 */ 
	public function setTransUnit($transUnit)
	{
		$fields = [
			'attributes', 'tags', 'children',
			'source', 'sourceLang', 'sourceAttributes', 'sourceChildren', 
			'target', 'targetLang', 'targetAttributes', 'targetChildren',
		];

		foreach ($fields as $field)
		{
			if (!empty($transUnit[$field]))
				$this->{$field}= $transUnit[$field];
		}
	}

	/**
	 * Recursive expand DOMNodeList to plain array with name/value/attributes/children elements
	 * 
	 * @param \DOMNodeList $nodesList
	 * @return array
	 */ 
	public function expandChildNodes(\DOMNodeList $nodesList)
	{
		$parsed = [];

		foreach ($nodesList as $node)
		{
			// skip text-only nodes
			if ($node->nodeType === \XML_TEXT_NODE)
				continue;

			$element = [
				'name' => $node->nodeName, 
				'value' => $node->nodeValue,
				'attributes' => [], 
				'children' => [],
			];

			if ($node->hasAttributes())
			{
				foreach ($node->attributes as $attr_key => $attr)
				{
					$element['attributes'][$attr_key] = $attr->nodeValue;
				}
			}

			if ($node->hasChildNodes())
			{
				$element['children'] = $this->expandChildNodes($node->childNodes);
			}

			$parsed[] = $element;
		}

		return $parsed;
	}
}