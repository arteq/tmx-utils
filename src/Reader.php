<?php
/**
 * TMX stream reader
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */
namespace ArteQ\Tmx;

class Reader extends TmxFile
{
	/**
	 * Source language code
	 * @var string
	 */ 
	private $srcLang;

	/**
	 * Target language code
	 * @var string
	 */
	private $trgLang;

	/**
	 * Create reader, check if file exists, read translation units
	 * 
	 * @param string $filePath
	 */ 
	public function __construct($filePath)
	{
		parent::__construct();

		if (!file_exists($filePath))
			throw new \Exception(sprintf("File '%s' does not exists.", $filePath));
		if (!is_readable($filePath))
			throw new \Exception(sprintf("File '%s' is not readable.", $filePath));
		
		$this->filePath = $filePath;
		$this->read();
	}

	public function getSourceLang()
	{
		return $this->srcLang;
	}

	public function getTargetLang()
	{
		return $this->trgLang;
	}

	/**
	 * Read segments from TMX file
	 */ 
	private function read()
	{
		// get first line of file
		$fh = fopen($this->filePath, 'rb');
		if ($fh === false)
			throw new \Exception(sprintf("Could not open file '%s'", $this->filePath));
		$firstLine = fgets($fh);
		if ($firstLine === false)
			throw new \Exception(sprintf("Could not read first line of file '%s'", $this->filePath));
		fclose($fh);

		$encoding = $this->detect_utf_encoding($firstLine);

		$reader = new \XMLReader();
		$reader->open($this->filePath, $encoding);

		$tuid = false;
		$xmlLang = false;

		while ($reader->read())
		{
			if ($reader->nodeType === \XMLReader::ELEMENT)
			{
				switch ($reader->localName)
				{
					case 'header':
						// read header attributes & properties
						$this->readHeader($reader->readOuterXml(), $this->header);

						break;

					case 'tu': 
						$tuid = $reader->getAttribute('tuid');

						// get target language code
						if (empty($this->trgLang))
							$this->parseTargetLangCode($reader->expand());

						$tu = new TransUnit($this->srcLang, $this->trgLang);
						if ($tu->read($reader->expand()))
							$this->transUnits[] = $tu;
 
						break;

					case 'tuv': 
						$xmlLang = $reader->xmlLang;
 
						break;

					case 'seg':
						if ($reader->read())
						{
							if (in_array($reader->nodeType, [\XMLReader::TEXT, \XMLReader::CDATA]))
							{
								if (!empty($tuid) && !empty($xmlLang))
									$this->data[$tuid][$xmlLang] = $reader->value;
							}
						}

						break;
				}
			}
		}

		$reader->close();
	}

	/**
	 * Read TMX file header element and recursive save it to destination array (attributes & children nodes)
	 * 
	 * @param string $input
	 * @param array $destination
	 */ 
	private function readHeader($input, &$destination)
	{
		$element = "<?xml version='1.0' encoding='UTF-8'?><root>" . $input . '</root>';
		$xml = simplexml_load_string($element);

		// get name
		$destination['name'] = $xml->children()[0]->getName();

		// get value (only if no futher children exists on that node)
		if (count($xml->children()[0]->children()) === 0)
			$destination['value'] = $xml->children()[0]->__toString();

		// get attributes
		foreach ($xml->children()[0]->attributes() as $attr_key => $attr_value)
		{
			$destination['attributes'][$attr_key] = (string)$attr_value;

			// get source language code
			if ($attr_key === 'srclang')
				$this->srcLang = (string)$attr_value;
		}

		// get children elements
		$i = 0;
		foreach ($xml->children()[0] as $name => $child)
		{
			$this->readHeader($child->asXML(), $destination['children'][$i++]);
		}
	}

	/**
	 * Set target language code from tuv attribute (other than already set source lang)
	 * 
	 * @param \DOMNode $tu
	 */ 
	private function parseTargetLangCode(\DOMNode $tu)
	{
		$document = new \DOMDocument(self::VERSION, self::ENCODING);
		$node = $document->importNode($tu, true);
		$document->appendChild($node);

		$xpath = new \DOMXPath($document);
		$xpath->registerNamespace('xml', 'XML');
		$tuvs = $xpath->evaluate('tuv');

		foreach ($tuvs as $tuv)
		{
			foreach ($tuv->attributes as $attr)
			{
				if ($attr->nodeName === 'xml:lang' && $attr->nodeValue !== $this->srcLang)
				{
					$this->trgLang = (string)$attr->nodeValue;

					return;
				}
			}
		}

		throw new \Exception('Cound not find target language');
	}
}