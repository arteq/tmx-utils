<?php
/**
 * TMX splitter, can be used to split very large single TMX file into smaller ones.
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */
namespace ArteQ\Tmx;

class Splitter extends TmxFile
{
	/**
	 * Input file name
	 * @var string
	 */ 
	private $inputFile;

	/**
	 * Output files directory
	 * @var string
	 */ 
	private $outputDir;

	/**
	 * Total number of written segments
	 * @var int
	 */ 
	private $segmentsWritten = 0;

	/**
	 * Total number of written bytes
	 * @var int
	 */ 
	private $bytesWritten = 0;

	/**
	 * Total number of written files
	 * @var int
	 */ 
	private $filesWritten = 0;

	/**
	 * Maximum number of segments per output file
	 * @var int
	 */ 
	private $limit = 1000;

	/**
	 * Input file header to be writen in output files as well
	 * @var string
	 */ 
	private $fileHeader;

	/**
	 * XML stream writer for output files
	 * @var \XMLWriter|null
	 */ 
	private $writer = null;

	/**
	 * List of libxml errors occured during read
	 * @var array
	 */ 
	private $errors = [];

	/**
	 * Create new splitter, check if input file exists, set output directory
	 * 
	 * @param string $inputFile
	 * @param string $outputDir
	 */ 
	public function __construct($inputFile, $outputDir = null)
	{
		parent::__construct();

		if (!$outputDir)
			$outputDir = trim(substr_replace($inputFile . ' ', '', strripos($inputFile, '/'), -1));

		if (!is_writable($outputDir))
			throw new \Exception('Output directory is not writable.');
		$this->outputDir = $outputDir;

		if (!is_readable($inputFile))
			throw new \Exception('File exist but not readable.');
		$this->inputFile = $inputFile;
	}

	/**
	 * Open input TMX file and save chunks of translation units to output TMX files
	 */ 
	public function split()
	{
		libxml_use_internal_errors(true);
		$reader = new \XMLReader();
		$reader->open($this->inputFile, self::ENCODING, \LIBXML_NOERROR | \LIBXML_NOWARNING);

		while ($data = $reader->read())
		{
			// read element
			if ($reader->nodeType === \XMLReader::ELEMENT)
			{
				if ($reader->localName === 'header')
				{					
					$this->fileHeader = $reader->readOuterXml();
				}
				elseif ($reader->localName === 'tu')
				{
					$this->writeSegment($reader->readOuterXml());
				}
			}
		}

		// flush last chunk
		$this->saveOutputFile();
		$reader->close();

		// check for errors
		$this->errors = libxml_get_errors();
	}

	/**
	 * Set segments limit per single output file
	 * 
	 * @param int $limit
	 */ 
	public function setLimit($limit)
	{
		$this->limit = $limit;
	}

	/**
	 * Append single translation unit to XML writer,
	 * create new output file if segments limit has been reached.
	 * 
	 * @param string $tu
	 */ 
	public function writeSegment($tu)
	{
		if ($this->writer === null)
			$this->createOutputFile();

		$this->writer->writeRaw($tu);
		$this->segmentsWritten++;

		if (($this->segmentsWritten % $this->limit) === 0)
			$this->saveOutputFile();		
	}

	/**
	 * Return split stats: total number of segments/files/bytes written.
	 * 
	 * @return array
	 */ 
	public function getStats()
	{
		return [
			'segmentsWritten' => $this->segmentsWritten,
			'bytesWritten' => $this->bytesWritten,
			'filesWritten' => $this->filesWritten,
		];
	}

	/**
	 * Return list of libxml errors
	 * 
	 * @return array
	 */ 
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Create new XML stream writer, add TMX file header
	 */ 
	private function createOutputFile()
	{
		// create new writer in memory
		$this->writer = new \XMLWriter();
		$this->writer->openMemory();
		$this->writer->startDocument(self::DOCUMENT, self::ENCODING);
		$this->writer->startElement('tmx');
		$this->writer->writeAttribute('version', self::VERSION);
		$this->writer->setIndentString("\t");
		$this->writer->setIndent(true);

		// write file header
		$this->writer->writeRaw("\n" . $this->fileHeader . "\n");

		// start body
		$this->writer->startElement('body');
	}

	/**
	 * Save XML data to file
	 */ 
	private function saveOutputFile()
	{
		// skip if not open for write
		if ($this->writer === null)
			return;

		// close body
		$this->writer->endElement();

		// close tmx
		$this->writer->endElement();

		// close xml document
		$this->writer->endDocument();

		// prepare filename
		$fileName = basename($this->inputFile, '.tmx');
		$fileName .= sprintf('_%03d', $this->filesWritten);
		$fileName .= '.tmx';

		// save XML to file
		$file = fopen($this->outputDir . \DIRECTORY_SEPARATOR . $fileName, 'w');
		fwrite($file, $this->writer->outputMemory(true));

		$this->writer = null;
		$this->filesWritten++;
		$this->bytesWritten += filesize($this->outputDir . \DIRECTORY_SEPARATOR . $fileName);
	}
}