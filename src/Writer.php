<?php
/**
 * TMX stream writer
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */
namespace ArteQ\Tmx;

class Writer extends TmxFile
{
	/**
	 * Default TMX <header> element attributes
	 * @var array
	 */
	private $headerAttrs = [
		'creationtool' => 'TMX utils',
		'creationtoolversion' => '1.3.0',
		'datatype' => 'xml',
		'o-tmf' => 'XLIFF',
		'segtype' => 'sentence',
		'adminlang' => 'en',
	];

	/**
	 * Writer object
	 * @var \XMLWriter
	 */ 
	private $writer;

	/**
	 * Number of written <tu> elements
	 * @var int
	 */ 
	private $tuWritten = 0;

	/**
	 * Create TMX writer
	 * 
	 * @param string $filePath
	 * @param array $headerAttrs
	 */ 
	public function __construct($filePath, $headerAttrs = [])
	{
		parent::__construct();
		$this->headerAttrs = array_merge($this->headerAttrs, $headerAttrs);
		$this->header = ['name' => 'header', 'attributes' => $this->headerAttrs];

		if (!file_exists($filePath))
		{
			$parent = dirname(__FILE__);
			if (!is_writable($parent))
				throw new \Exception(sprintf("Directory '%s' is not writable.", $parent));

			// create empty file
			$fh = fopen($filePath, 'w');
			fclose($fh);
			unset($fh);
		}

		if (!is_writable($filePath))
			throw new \Exception(sprintf("File '%s' is not writable.", $filePath));
		
		$writer = new \XMLWriter();
		$writer->openUri($filePath);
		$this->writer = $writer;
	}

	/**
	 * Write data to TMX file
	 */ 
	public function write()
	{
		$this->writeStart();
		
		foreach ($this->data as $tuid => $tuvs)
		{
			$this->writeTu($tuid, $tuvs);
		}

		$this->writeEnd();		
	}

	/**
	 * Start TMX writer, write header
	 */ 
	public function writeStart()
	{
		$this->writer->startDocument(self::DOCUMENT, self::ENCODING);
		$this->writer->startElement('tmx');
		$this->writer->writeAttribute('version', self::VERSION);
		$this->writer->setIndentString("\t");
		$this->writer->setIndent(true);

		// write header
		$this->writeElement($this->header);

		$this->writer->startElement('body');
	}

	/**
	 * Recursive write elements with attributes and child elements
	 * 
	 * @param array $element
	 */ 
	public function writeElement($element)
	{
		if (isset($element['name']))
		{
			$this->writer->startElement($element['name']);

			// once inside <seg> element don't add any indent thay may broke segment formatting
			if ($element['name'] === 'seg')
				$this->writer->setIndent(false);
		}

		if (!empty($element['attributes']))
		{
			foreach ($element['attributes'] as $key => $value)
			{
				$this->writer->writeAttribute($key, $value);
			} 
		}

		if (!empty($element['children']))
		{
			foreach ($element['children'] as $child)
			{
				$this->writeElement($child);
			}
		}

		if (isset($element['value']))
			$this->writer->text($element['value']);

		if (isset($element['name']))
		{
			$this->writer->endElement();

			// on leaving <seg> element revert to indent
			if ($element['name'] === 'seg')
			{
				$this->writer->writeRaw("\n");
				$this->writer->setIndent(true);
			}
		}
	}

	/**
	 * Write single <tu> element to file
	 * 
	 * @param string $tuid
	 * @param array $tuvs
	 */ 
	public function writeTu($tuid, $tuvs)
	{
		$this->writer->startElement('tu');
		$this->writer->writeAttribute('tuid', $tuid);

		if (isset($tuvs['_attributes']))
		{
			foreach ($tuvs['_attributes'] as $attrName => $attrValue)
			{
				$this->writer->writeAttribute($attrName, $attrValue);
			}

			unset($tuvs['_attributes']);
		}

		if (isset($tuvs['_properties']))
		{
			foreach ($tuvs['_properties'] as $propType => $propValue)
			{
				$this->writer->startElement('prop');
				$this->writer->writeAttribute('type', $propType);
				$this->writer->text($propValue);
				$this->writer->endElement();
			}

			unset($tuvs['_properties']);
		}

		foreach ($tuvs as $xmlLang => $value)
		{
			$this->writer->startElement('tuv');
			$this->writer->writeAttribute('xml:lang', $xmlLang);
			$this->writer->writeElement('seg', $value);
			$this->writer->endElement();
		}

		$this->writer->endElement();

		$this->tuWritten++;

		// save data to file every 1000 elements
		if (($this->tuWritten % 1000) === 0)
			$this->writer->flush();
	}

	/**
	 * Write closing elements, free resource
	 */ 
	public function writeEnd()
	{
		// close body
		$this->writer->endElement();

		// close tmx
		$this->writer->endElement();

		// close xml document
		$this->writer->endDocument();
		
		// free resource
		$this->writer->flush();
		unset($this->writer);		
	}
}