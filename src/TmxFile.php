<?php
/**
 * TMX file base class
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2020. All rights reserved.
 */
namespace ArteQ\Tmx;

// Unicode BOM is U+FEFF, but after encoded, it will look like this.
define ('UTF32_BIG_ENDIAN_BOM', chr(0x00) . chr(0x00) . chr(0xFE) . chr(0xFF));
define ('UTF32_LITTLE_ENDIAN_BOM', chr(0xFF) . chr(0xFE) . chr(0x00) . chr(0x00));
define ('UTF16_BIG_ENDIAN_BOM', chr(0xFE) . chr(0xFF));
define ('UTF16_LITTLE_ENDIAN_BOM', chr(0xFF) . chr(0xFE));
define ('UTF8_BOM', chr(0xEF) . chr(0xBB) . chr(0xBF));

abstract class TmxFile
{
	/**
	 * XML file encoding
	 * @var string
	 */ 
	const ENCODING = 'UTF-8';

	/**
	 * XML document version
	 * @var string
	 */  
	const DOCUMENT = '1.0';

	/**
	 * TMX file version
	 * @var string
	 */ 
	const VERSION = '1.4';

	/**
	 * TMX file path 
	 * @var string
	 */ 
	protected $filePath = '';

	/**
	 * Header data
	 * @var array
	 */ 
	protected $header = [];

	/**
	 * Array with simplified transUnits segments as plain text
	 * @var array
	 */ 
	protected $data = [];

	/**
	 * Array with full transUnits, tags, attributes and properties
	 * @var array
	 */ 
	protected $transUnits = [];

	/**
	 * Check for required libxml extension
	 */ 
	public function __construct()
	{
		if (!class_exists('XMLReader') || !class_exists('XMLWriter'))
			throw new \Exception('PHP extension libxml is required');
	}

	/**
	 * Delete single translation unit (whole or only certain language)
	 * 
	 * @param string $tuid
	 * @param string $xmlLang
	 */ 
	public function delete($tuid, $xmlLang = '')
	{
		if ($xmlLang)
		{
			if (isset($this->data[$tuid]) && isset($this->data[$tuid][$xmlLang]))
			{
				unset($this->data[$tuid][$xmlLang]);

				if (empty($this->data[$tuid]))
				{
					unset($this->data[$tuid]);
				}
			}
		}
		else
		{
			if (isset($this->data[$tuid]))
			{
				unset($this->data[$tuid]);
			}
		}
	}

	/**
	 * Add additional attribute to <tu> element
	 * 
	 * @param string $tuid
	 * @param string $name
	 * @param string $value
	 */ 
	public function setAttribute($tuid, $name, $value)
	{
		if (!isset($this->data[$tuid]))
			throw new \Exception(sprintf("No translation unit with id: '%s'.", $tuid));

		$this->data[$tuid]['_attributes'][$name] = $value;
	}

	/**
	 * Add additional property to <tu> element
	 * 
	 * @param string $tuid
	 * @param string $name
	 * @param string $value
	 */ 
	public function setProperty($tuid, $name, $value)
	{
		if (!isset($this->data[$tuid]))
			throw new \Exception(sprintf("No translation unit with id: '%s'.", $tuid));

		$this->data[$tuid]['_properties'][$name] = $value;
	}

	/**
	 * Set single translation unit segment
	 * 
	 * @param string $tuid
	 * @param string $xmlLang
	 * @param string $value
	 */ 
	public function set($tuid, $xmlLang, $value = '')
	{
		if (empty($tuid))
			throw new \Exception('Missing translation unit id.');

		if (empty($xmlLang))
			throw new \Exception('Missing translation unit language code.');

		$this->data[$tuid][$xmlLang] = $value;
	}

	/**
	 * Set multiple translation units from array
	 * 
	 * @param array $data
	 */ 
	public function setArray(array $data)
	{
		foreach ($data as $tu)
		{
			if (is_array($tu))
			{
				if (count($tu) > 2)
				{
					$this->set($tu[0], $tu[1], $tu[2]);
				}
			}
		}
	}
	
	/**
	 * Get all translation units, single translation unit for given id 
	 * or single segment for given language code.
	 * 
	 * @param string|bool $tuid
	 * @param string|bool $xmlLang
	 * @return array|string
	 */  
	public function get($tuid = false, $xmlLang = false)
	{
		if ($xmlLang && $tuid)
			return (isset($this->data[$tuid][$xmlLang])) ? $this->data[$tuid][$xmlLang] : false;
		elseif ($tuid)
			return (isset($this->data[$tuid])) ? $this->data[$tuid] : false;
		
			return $this->data;
	}

	/**
	 * Get all translation units (full data with properties & attributes)
	 * 
	 * @return array
	 */ 
	public function getTransUnits()
	{
		return $this->transUnits;
	}

	/**
	 * Get all translation units for given language
	 * 
	 * @param string $xmlLang
	 * @return array
	 */ 
	public function getLang($xmlLang)
	{
		$data = [];

		foreach ($this->data as $tuid => $tuv)
		{
			foreach ($tuv as $lang => $value)
			{
				if ($xmlLang === $lang)
				{
					$data[$tuid][$lang] = $this->data[$tuid][$lang];
				}
			}
		}

		return $data;
	}

	/**
	 * Return header data (header element attributes & child properties)
	 * 
	 * @return array
	 */ 
	public function getHeader()
	{
		return $this->header;
	}

	/**
	 * Set header data (header element attributes & child properties)
	 * 
	 * @param array $header
	 */ 
	public function setHeader($header)
	{
		$this->header = $header;
	}

	/**
	 * Try to detect bogus encoding (UTF-16 as returned by API)
	 *  
	 * @param  string $text
	 * @return string [UTF-8|UTF-16]
	 */
	public function detect_utf_encoding($text) 
	{
		$first2 = substr($text, 0, 2);
		$first3 = substr($text, 0, 3);
		$first4 = substr($text, 0, 3);

		if ($first3 === UTF8_BOM) return 'UTF-8';
		elseif ($first4 === UTF32_BIG_ENDIAN_BOM) return 'UTF-32';
		elseif ($first4 === UTF32_LITTLE_ENDIAN_BOM) return 'UTF-32';
		elseif ($first2 === UTF16_BIG_ENDIAN_BOM) return 'UTF-16';
		elseif ($first2 === UTF16_LITTLE_ENDIAN_BOM) return 'UTF-16';

		 return 'UTF-8';
	}
}